<?php
require_once 'Classes/Menu.php';
require_once 'Classes/Food.php';
require_once 'Classes/Drink.php';
require_once 'Classes/HtmlWriter.php';
require_once 'menu.php';

echo '<pre><h1>Напитки</h1></pre>';

foreach ($drinkArray as $drinks) {
    $drink = new Drink($drinks['title'], $drinks['price'],$drinks['volume']);
    $htmlWriter = new HtmlWriter();
    echo $htmlWriter->writeHtmlDrink($drink);
}
