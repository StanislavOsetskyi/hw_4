<?php


class Food extends Menu
{
    public $composition = '';
    public $vegetarian = '';
    public $weight = 1;

    public function __construct($title, $price,$composition,$vegetarian,$weight)
    {
        parent::__construct($title, $price);
        $this->composition = $composition;
        $this->vegetarian = $vegetarian;
        $this->weight = $weight;
    }
    public function getInfo()
    {
        $str = parent::getInfo();

        $vegetarian = "Нет";
        if ($this->vegetarian){
            $vegetarian = "Да";
        }

        $str .=' '.'Состав:'. $this->composition;
        $str .= 'Для вегетарианцев:' . $vegetarian;
        $str .=' '.'Вес:'. $this->weight . 'Гр.';
        return $str;
    }
}