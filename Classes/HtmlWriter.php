<?php


class HtmlWriter
{
    public function writeHtmlDish(Food $dish){

        $vegetarian = "Нет";
        if ($dish->vegetarian){
            $vegetarian = "Да";
        }

        $str = '<div>';
        $str .= '<ol>';
        $str .= '<li>' .  $dish->title . '</li>';
        $str .= '<li> Цена:' . $dish->price . 'Грн.' . '</li>';
        $str .= '<li> Состав:' . $dish->composition . '</li>';
        $str .= '<li> Вегетарианское:' . $vegetarian . '</li>';
        $str .='<li> Вес:' . $dish->weight . 'Гр.' . '</li>';
        $str .= "</ol>";
        $str .= '<div>';
        return $str;
    }
    public function writeHtmlDrink(Drink $drink)
    {
        

        $str = '<div>';
        $str .= '<ol>';
        $str .= '<li>' .  $drink->title . '</li>';
        $str .= '<li> Цена:' . $drink->price . 'Грн.' . '</li>';
        $str .= '<li> ' . $drink->volume .'мл.' . '</li>';
        $str .= "</ol>";
        $str .= '<div>';
        return $str;
    }

}