<?php


class Drink extends Menu
{
    public $volume;

    public function __construct($title, $price,$volume)
    {
        parent::__construct($title, $price);
        $this->volume = $volume;
    }

    public function getInfo()
    {
        $str = parent::getInfo();
        $str .= ' ' . $this->volume . 'мл.';
        return $str;
    }
}