<?php


class Menu
{
    public $title = '';
    public $price = 1 ;
    public function __construct($title,$price)
    {
        $this->title = $title;
        $this->price = $price;
    }

    public function getInfo(){
        $str = '';
        $str .= $this->title . ' - ';
        $str .= $this->price .' '.'Грн.';
        return $str;
    }
}