<?php
require_once 'Classes/Menu.php';
require_once 'Classes/Food.php';
require_once 'Classes/Drink.php';
require_once 'Classes/HtmlWriter.php';
require_once 'menu.php';

echo '<pre><h1>Вегетарианские блюда</h1></pre>';

foreach ($foodArray as $food) {
    if ($food['vegetarian'] == true) {

        $dish = new Food($food['title'], $food['price'], $food['composition'], $food['vegetarian'], $food['weight']);
        $htmlWriter = new HtmlWriter();
        echo $htmlWriter->writeHtmlDish($dish);
    }
}
