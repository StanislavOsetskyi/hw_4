<?php
$foodArray = [
    [
        'title'=>'Салат греческий',
        'price'=>100,
        'composition'=>'овощи,сыр,маслины',
        'vegetarian'=>true,
        'weight'=>200
    ],
    [
        'title'=>'Медальоны',
        'price'=>140,
        'composition'=>'Телятина,грибы,сливочный соус',
        'vegetarian'=>false,
        'weight'=>250
    ],
    [
        'title'=>'Каре ягненка',
        'price'=>210,
        'composition'=>'Мясо ягненка',
        'vegetarian'=>false,
        'weight'=>200
    ],
    [
        'title'=>'Картофель по-селянски',
        'price'=>50,
        'composition'=>'Картофель, зелень',
        'vegetarian'=>true,
        'weight'=>250
    ],
];

$drinkArray = [
    [
        'title'=>'Вино',
        'price'=>400,
        'volume'=>750
    ],
    [
        'title'=>'Мохито',
        'price'=>40,
        'volume'=>300
    ],
    [
        'title'=>'Кофе Американо',
        'price'=>35,
        'volume'=>75
    ],
    [
        'title'=>'Сок в ассортименте',
        'price'=>40,
        'volume'=>300
    ],
];
