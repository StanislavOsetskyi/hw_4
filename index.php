<?php
require_once 'Classes/Menu.php';
require_once 'Classes/Food.php';
require_once 'Classes/Drink.php';
require_once 'Classes/HtmlWriter.php';
require_once 'menu.php';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Menu</title>
</head>
<body>
<h1>Меню</h1>
<div>
    <table>
            <tr>
                <?php foreach ($foodArray as $food):?>
                <td><?php $dish = new Food($food['title'], $food['price'],
                        $food['composition'], $food['vegetarian'], $food['weight']);
                    $htmlWriter = new HtmlWriter();
                   echo $htmlWriter->writeHtmlDish($dish); ?></td>
                <?php endforeach;?>
            </tr>
        <tr>
            <?php foreach ($drinkArray as $drinks):?>
                <td><?php $drink = new Drink($drinks['title'], $drinks['price'],$drinks['volume']);
                    $htmlWriter = new HtmlWriter();
                    echo $htmlWriter->writeHtmlDrink($drink); ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <td>
                <form action="drinks.php">
                    <button>Напитки</button>
                </form>
                <form action="vegetarian.php">
                    <button>Вегетарианские блюда</button>
                </form>
            </td>

        </tr>
    </table>
</div>
</body>
</html>
